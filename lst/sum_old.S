.data /* 64 бита */
fmt_debug: .string "%02d) n = %d, ds = %0.8f, s = %0.8f, A = %d\n"
fmt_out: .string "s (loop final) = %0.8f\ns (theory lim) = %0.8f\neps = %e\n"
eps: .double 1e-6
six: .double 6
.global main

main: // sum 1/(i^2)
    sub $3*8, %rsp // 32-8 = 24 байта — выравнивание!!! и заодно место для локальных переменных
    
#define mem_s_to_print (%rsp)    
#define mem_i 8(%rsp)  
#define mem_n 12(%rsp)
#define mem_m 16(%rsp)
#define mem_step 20(%rsp)
// итого 1×double + 4×long = 8 + 4*4 = 24 байта
   
first_n_init:  
// сумма до n
    movl $32, mem_n
    mov $1, %eax
    mov mem_n, %ecx    
    movl $0, mem_step
    
    // расчёт s от A до C включительно
    // счётчик цикла mem_i в памяти, чтобы загружать его в стек FPU
    fldz        // st: s=0
    mov %ecx, mem_i    // C → i
next_i_0:       // st: s
    fildl mem_i // st: i,       s
    fld %st(0)  // st: i,       i,      s
    fmulp       // st: i^2,     s
    fld1        // st: 1,       i^2,    s
    fdivp       // st: 1/i^2,   s
    faddp       // st: 1/i^2+s   
    decl mem_i
    cmpl %eax, mem_i
    jge next_i_0 // i-A ≥ 0   
    // i-A < 0,    st: s


// Начало следующего большого шага (вычисления суммы от A=n+1 до C=m)    
next_n_init: 
// сумма от n+1 до m   
    mov mem_n, %eax // n → A
    mov %eax, %ecx  // n → C
    inc %eax        // n+1 → A
    // Вычисление m(n)
    shl  $1, %ecx   // m=2n → C
    mov %ecx, mem_m // сохраняем m в память
    mov %ecx, mem_n // m → n, всё равно до следующего большого шага оно не нужно
    incl mem_step   // увеличиваем номер большого шага (для красоты и возможного контроля расходимости)
 
    // расчёт ds от A до C включительно
    // счётчик цикла mem_i в памяти, чтобы загружать его в стек FPU
    // цикл такой же, что и выше (начиная с метки next_i_0), только выход в другое место — можно подпрограммой!
                // st: s        
    fldz        // st: ds=0,    s
    mov %ecx, mem_i    // C → i
next_i:         // st: ds,      s    
    fildl mem_i // st: i,       ds,     s
    fld %st(0)  // st: i,       i,      ds,     s
    fmulp       // st: i^2,     ds,     s
    fld1        // st: 1,       i^2,    ds,     s
    fdivp       // st: 1/i^2,   ds,     s
    faddp       // st: 1/i^2+ds, s    
    decl mem_i
    cmpl %eax, mem_i
    jge next_i // i-A ≥ 0   
    // i-A < 0,    st: ds,    s
    
    lea fmt_debug(%rip), %rdi
    mov mem_step, %esi
    mov mem_n, %edx
    fstl mem_s_to_print
    vmovsd mem_s_to_print, %xmm0
    fxch %st(1)
    fstl mem_s_to_print
    fxch %st(1)
    vmovsd mem_s_to_print, %xmm1
    mov %eax, %ecx
    mov $2, %al // 2×xmm
    call printf     
    
    fldl eps(%rip)              // st: eps,     ds,     s
    fucomip %st(1), %st(0)      // st: ds,      s            flags: eps-ds
    faddp                       // st: ds+s
    jbe next_n_init     // eps-ds ≤ 0
            
            
    // st: s
    fstpl mem_s_to_print

    lea fmt_out(%rip), %rdi
    vmovsd mem_s_to_print, %xmm0
    
    fldpi
    fldpi
    fmulp
    fldl six(%rip)
    fdivrp
    fstpl mem_s_to_print

    vmovsd mem_s_to_print, %xmm1
    
    vmovsd eps(%rip), %xmm2
    mov $3, %al // 3×xmm
    call printf 

    
    add $3*8, %rsp

    xor %eax, %eax
    ret 
  
/*  
$ g++ sum_short_2.S && ./a.out 
n = 64, ds = 0.01526324, s = 1.61416726, A = 33
n = 128, ds = 0.00772150, s = 1.62943050, A = 65
n = 256, ds = 0.00388343, s = 1.63715200, A = 129
n = 512, ds = 0.00194741, s = 1.64103544, A = 257
n = 1024, ds = 0.00097513, s = 1.64298285, A = 513
n = 2048, ds = 0.00048792, s = 1.64395798, A = 1025
n = 4096, ds = 0.00024405, s = 1.64444590, A = 2049
n = 8192, ds = 0.00012205, s = 1.64468996, A = 4097
n = 16384, ds = 0.00006103, s = 1.64481200, A = 8193
n = 32768, ds = 0.00003052, s = 1.64487303, A = 16385
n = 65536, ds = 0.00001526, s = 1.64490355, A = 32769
n = 131072, ds = 0.00000763, s = 1.64491881, A = 65537
n = 262144, ds = 0.00000381, s = 1.64492644, A = 131073
n = 524288, ds = 0.00000191, s = 1.64493025, A = 262145
n = 1048576, ds = 0.00000095, s = 1.64493216, A = 524289
s = 1.64493311
  → 1.64493407
  */
