// Макрос для компенсации искажения имён в Mac OS X (и в MS Windows 32, но в этой программе неактуально)
#ifdef __APPLE__
#define FNNAME(s) _##s
#elif _WIN64
#define FNNAME(s) s
#elif _WIN32
#define FNNAME(s) _##s
#else
#define FNNAME(s) s
#endif 

// struct TSomeStruct
// {
//     char Tag;
//     char align[3];
//     int  Val;
// } s;


.globl FNNAME(init)

.func
FNNAME(init): 

    // extern "C" void init(TSomeStruct *p);
    // соглашение System V amd64 
    // указатель p в %rdi
    
    sub $8, %rsp
    
    movb $'b', (%rdi)  
    movl $14, 4(%rdi)  
   
    add $8, %rsp
    ret 

    // суффиксы b/l обязательны (один операнд непосредственный, другой в памяти — без суффикса невозможно установить их размер, а значит, и разрядность команды)
    // смещение Val равно 4 при любой кратности выравнивания, так как суммарный размер предыдущих полей равен 4 — максимальному выравниванию int
    // выравнивать/восстанавливать %rsp здесь не обязательно, так как нет ни вложенных вызовов, ни xmm-команд — но пусть будет, чтобы плохому не учились

.endfunc
