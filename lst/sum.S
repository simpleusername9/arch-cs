.data /* 64 бита, System V */
eps: .double 1e-6
six: .double 6
fmt_debug: .string "%02d) ds = %0.8lf, s = %0.8lf, n = %d, n+1 = %d,  m = %d\n" // формат для отладочного вывода (n+1 — это регистр A, где оно должно быть)
fmt_out: .string "s (loop final) = %0.8lf\ns (theory lim) = %0.8lf\neps = %le\n" // формат для финального вывода

// Подпрограмма (не функция, ибо соглашение нестандартное) serie_fragment()
// вычисляет кусок ряда от A (i=eax) до C (i=ecx > A), обе границы включаемые,
// сложение — с конца (то есть начиная с C, от меньших по модулю членов к бОльшим)
// — входные параметры — в регистрах A и C
// — результат (вычисленное значение суммы куска ряда) оставляет на вершине стека FPU
// использует стек FPU на глубину 3, не очищает (если в стеке FPU было что-то ещё, остаётся под результатом)
// не меняет регистров CPU
// меняет флаги CPU
serie_fragment:
    sub $8, %rsp // выравнивание и место для локальной переменной mem_i (счётчик цикла mem_i в памяти, чтобы загружать его в стек FPU)
    #define mem_i (%rsp)

    fldz        // st: s=0
    mov %ecx, mem_i    // C → i
next_i:         // st: s
    fildl mem_i // st: i,       s
    fld %st(0)  // st: i,       i,      s
    fmulp       // st: i^2,     s
    fld1        // st: 1,       i^2,    s
    fdivp       // st: 1/i^2,   s
    faddp       // st: 1/i^2+s
    decl mem_i
    cmpl %eax, mem_i
    jge next_i // i-A ≥ 0  — i≥A

    // i-A < 0,    st: s — выход
    add $8, %rsp
    #undef mem_i
    ret
    
// Головная функция main()    

.global main
.func
main: // sum 1/(i^2)
    sub $3*8, %rsp // 32-8 = 24 байта — выравнивание для vmovsd и printf!!! и заодно место для локальных переменных

#define mem_s_to_print (%rsp)
#define mem_n 8(%rsp)
#define mem_m 12(%rsp)
#define mem_step 16(%rsp)
// итого 1×double + 3×long = 8 + 3*4 = 20 байтов
// можно было n/m/step хранить также в неизменяемых регистрах (а сами регистры сохранять/восстанавливать в прологе/эпилоге)

first_n_init:
// сумма до n
    movl $32, mem_n
    mov $1, %eax
    mov mem_n, %ecx
    movl $0, mem_step

    // расчёт s от A до C включительно
    call serie_fragment
    // st: s


// Начало следующего большого шага (вычисления суммы от A=n+1 до C=m)
next_n_init:
// сумма от n+1 до m
// A перезаписывается значением из памяти
    mov mem_n, %eax // n → A
    mov %eax, %ecx  // n → C
    inc %eax        // n+1 → A

    // Вычисление m(n)
    shl  $1, %ecx   // m=2n → C
    mov %ecx, mem_m // сохраняем m в память
    //mov %ecx, mem_n // сохраняем новое n в память
    incl mem_step   // увеличиваем номер большого шага (для красоты и возможного контроля расходимости)

    // Расчёт ds от A=n+1 до C=m включительно
    call serie_fragment
    // st: ds,    s

    // Отладочный вывод, s копируем, но не извлекаем из стека
    lea fmt_debug(%rip), %rdi
    mov mem_step, %esi
    mov mem_n, %edx
    mov %eax, %ecx
    mov mem_m, %r8d
    fstl mem_s_to_print
    vmovsd mem_s_to_print, %xmm0 // ds
    fxch %st(1)
    fstl mem_s_to_print
    fxch %st(1)
    vmovsd mem_s_to_print, %xmm1 // s
    mov $2, %al // 2 xmm-параметра printf
    call printf
    
    // Замена m→n. Так как значение в C уже испорчено, то mem_m→mem_n через промежуточный регистр
    mov mem_m, %eax
    mov %eax, mem_n

    // Контроль точности и расчёт нового s (ds+s→s)
    fldl eps(%rip)              // st: eps,     ds,     s
    fucomip %st(1), %st(0)      // st: ds,      s            flags: eps-ds
    faddp                       // st: ds+s
    jbe next_n_init     // eps-ds ≤ 0 (faddp флаги CPU не меняет!)


    // st: s
    // Финальный вывод, s извлекаем из стека
    fstpl mem_s_to_print

    lea fmt_out(%rip), %rdi
    vmovsd mem_s_to_print, %xmm0

    // Расчёт теоретического значения суммы: π^2/6
    fldpi
    fldpi
    fmulp
    fldl six(%rip)
    fdivrp
    fstpl mem_s_to_print

    vmovsd mem_s_to_print, %xmm1
    vmovsd eps(%rip), %xmm2        
    mov $3, %al // 3 xmm-параметра printf
    call printf


    add $3*8, %rsp

    xor %eax, %eax
    ret
.endfunc

/*
$ g++ sum.S && ./a.out 
01) ds = 0.01526324, s = 1.61416726, n = 32, n+1 = 33,  m = 64
02) ds = 0.00772150, s = 1.62943050, n = 64, n+1 = 65,  m = 128
03) ds = 0.00388343, s = 1.63715200, n = 128, n+1 = 129,  m = 256
04) ds = 0.00194741, s = 1.64103544, n = 256, n+1 = 257,  m = 512
05) ds = 0.00097513, s = 1.64298285, n = 512, n+1 = 513,  m = 1024
06) ds = 0.00048792, s = 1.64395798, n = 1024, n+1 = 1025,  m = 2048
07) ds = 0.00024405, s = 1.64444590, n = 2048, n+1 = 2049,  m = 4096
08) ds = 0.00012205, s = 1.64468996, n = 4096, n+1 = 4097,  m = 8192
09) ds = 0.00006103, s = 1.64481200, n = 8192, n+1 = 8193,  m = 16384
10) ds = 0.00003052, s = 1.64487303, n = 16384, n+1 = 16385,  m = 32768
11) ds = 0.00001526, s = 1.64490355, n = 32768, n+1 = 32769,  m = 65536
12) ds = 0.00000763, s = 1.64491881, n = 65536, n+1 = 65537,  m = 131072
13) ds = 0.00000381, s = 1.64492644, n = 131072, n+1 = 131073,  m = 262144
14) ds = 0.00000191, s = 1.64493025, n = 262144, n+1 = 262145,  m = 524288
15) ds = 0.00000095, s = 1.64493216, n = 524288, n+1 = 524289,  m = 1048576
s (loop final) = 1.64493311
s (theory lim) = 1.64493407
eps = 1.000000e-06
*/

