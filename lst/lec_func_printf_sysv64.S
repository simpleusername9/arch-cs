// Макрос для компенсации искажения имён в Mac OS X (и в MS Windows 32, но в этой программе неактуально)
#ifdef __APPLE__
#define FNNAME(s) _##s
#elif _WIN64
#define FNNAME(s) s
#elif _WIN32
#define FNNAME(s) _##s
#else
#define FNNAME(s) s
#endif 

.data 
    fmt: .string "x = %d, y = %d\n"
    x:   .int +122
    
.text
.globl FNNAME(main)

FNNAME(main):        
    push %rbp 
    
    lea fmt(%rip), %rdi
    movslq x(%rip), %rsi
    mov $-13, %rdx
    mov $0, %al   
    call FNNAME(printf)
    
    pop %rbp 
    xor %eax, %eax 
    ret 
